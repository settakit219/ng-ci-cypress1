const webpack = require('webpack');
require('dotenv').config();

module.exports = {
    plugins: [
        new webpack.DefinePlugin({
            $ENV: {
                API: JSON.stringify(process.env.API),
                API2: JSON.stringify(process.env.API2)
            }
        })
    ]
}